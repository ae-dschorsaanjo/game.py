# game.py

It is a quite simple "game" 
game.py is a simple program to do some math for fun, practicing math or just killing some time (e.g. in a boring class  -- though playing during a class is not recommended).

The original version of it was written in C# (with a lot of bugs and extremely ugly and amateur coding "style"), and there is or will be a new version written in Pascal.

# How to ~~use~~ play

It has two game modes, the "finite" and "infinite" game. You can choose between these modes when you type the number of games. You should type an 0 for an infinite game and type a number between 1 and 1024 for a finite game.
This game has two game modes, "finite" and "infinite". The difference is simple; a finite game will give you _number of games_ expressions. An infinite game otherwise give you expressions as far as you type correct answers.

If you'll find a bug or something irritating in the code, please let me know.
